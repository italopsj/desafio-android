package concrete.com.br.desafioandroid.service.interceptor;

import android.util.Log;

import java.io.IOException;

import concrete.com.br.desafioandroid.utils.AndroidUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by italopaolo on 17/01/2018.
 */

public class OfflineResponseCacheInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        if (Boolean.valueOf(request.header("ApplyOfflineCache"))) {
            Log.i(getClass().toString(), "Offline cache applied");
            if (!AndroidUtils.isNetworkAvailable()) {
                request = request.newBuilder()
                        .removeHeader("ApplyOfflineCache")
                        .header("Cache-Control",
                                "public, only-if-cached, max - stale = " + 2419200)
                        .build();
            }
        } else
            Log.i(getClass().toString(), "Offline cache not applied");

        return chain.proceed(request);
    }
}

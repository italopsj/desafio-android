package concrete.com.br.desafioandroid.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import concrete.com.br.desafioandroid.R;
import concrete.com.br.desafioandroid.base.BaseViewModelActivity;
import concrete.com.br.desafioandroid.databinding.ActivityHomeBinding;
import concrete.com.br.desafioandroid.viewModel.HomeViewModel;
import rx.subscriptions.CompositeSubscription;

public class HomeActivity extends BaseViewModelActivity<ActivityHomeBinding, HomeViewModel> {

    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        if (viewModel == null) {
            viewModel = new HomeViewModel();
        }

        binding.setViewModel(viewModel);

        setUpToolbar(getString(R.string.home_toolbar_title), false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!subscriptions.hasSubscriptions()) {
            subscriptions.add(viewModel
                    .repositorySubject
                    .subscribe(response -> {
                        Intent intent = new Intent(this, RepositoryDetailActivity.class);
                        intent.putExtra(getString(R.string.repository_tag), response);
                        startActivity(intent);
                    }));
        }
    }

}

package concrete.com.br.desafioandroid.base;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by italopaolo on 16/01/2018.
 */

public class DataBinder {

    @BindingAdapter({"android:visibility"})
    public static void setVisibility(View view, boolean isVisible) {

        int visibility = isVisible ? View.VISIBLE : View.GONE;
        view.setVisibility(visibility);
    }

    @BindingAdapter({"imageUrl", "placeholder"})
    public static void setImageUrl(ImageView imageView, String url, Drawable placeholder) {
        Context context = imageView.getContext();

        Picasso.with(context)
                .load(url)
                .placeholder(placeholder)
                .resize(300, 300)
                .centerCrop()
                .noFade()
                .into(imageView);
    }

    @BindingAdapter("scrollListener")
    public static void setOnScrollListener(RecyclerView recyclerView, RecyclerView.OnScrollListener listener){
        recyclerView.addOnScrollListener(listener);
    }
}

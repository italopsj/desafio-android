package concrete.com.br.desafioandroid;

import android.support.multidex.MultiDexApplication;

/**
 * Created by italopaolo on 15/01/2018.
 */

public class App extends MultiDexApplication {

    private static App instance;

    public static App get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }
}

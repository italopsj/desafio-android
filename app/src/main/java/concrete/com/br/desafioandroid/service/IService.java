package concrete.com.br.desafioandroid.service;

import java.util.List;

import concrete.com.br.desafioandroid.model.PullRequest;
import concrete.com.br.desafioandroid.model.Search;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by italopaolo on 15/01/2018.
 */

public interface IService {

    @Headers("ApplyOfflineCache: true")
    @GET("search/repositories")
    Observable<Search> searchRepositories(@Query("q") String language,
                                          @Query("sort") String sort,
                                          @Query("page") int page);

    @GET("repos/{owner}/{repository}/pulls")
    Observable<List<PullRequest>> getPulls(@Path("owner") String owner,
                                           @Path("repository") String repository);

}

package concrete.com.br.desafioandroid.item;

import com.genius.groupie.Item;

import concrete.com.br.desafioandroid.R;
import concrete.com.br.desafioandroid.databinding.ItemRepositoryBinding;
import concrete.com.br.desafioandroid.model.Repository;
import concrete.com.br.desafioandroid.utils.AndroidUtils;
import concrete.com.br.desafioandroid.viewModel.HomeViewModel.OnRepositoryItemListener;
import concrete.com.br.desafioandroid.viewModel.RepositoryViewModel;

/**
 * Created by italopaolo on 16/01/2018.
 */

public class ItemRepository extends Item<ItemRepositoryBinding> {

    private RepositoryViewModel viewModel;

    public ItemRepository(Repository repositoryItem, OnRepositoryItemListener listener) {
        viewModel = new RepositoryViewModel(repositoryItem, listener);
    }

    @Override
    public int getLayout() {
        return R.layout.item_repository;
    }

    @Override
    public void bind(ItemRepositoryBinding viewBinding, int position) {
        AndroidUtils.setScaleAnimation(viewBinding.getRoot());
        viewBinding.setViewModel(viewModel);
    }

}

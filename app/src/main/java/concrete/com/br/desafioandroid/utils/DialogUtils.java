package concrete.com.br.desafioandroid.utils;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by italopaolo on 15/01/2018.
 */

public class DialogUtils {

    public static void alertDialog(Context context, String message) {
        new MaterialDialog.Builder(context)
                .content(message)
                .positiveText(android.R.string.ok)
                .show();
    }
}

package concrete.com.br.desafioandroid.viewModel;

import android.databinding.ObservableField;

import concrete.com.br.desafioandroid.base.BaseViewModel;
import concrete.com.br.desafioandroid.model.Repository;
import concrete.com.br.desafioandroid.viewModel.HomeViewModel.OnRepositoryItemListener;

/**
 * Created by italopaolo on 16/01/2018.
 */

public class RepositoryViewModel extends BaseViewModel {

    public final ObservableField<String> repositoryName = new ObservableField<>();
    public final ObservableField<String> description = new ObservableField<>();
    public final ObservableField<String> ownerName = new ObservableField<>();
    public final ObservableField<String> ownerAvatar = new ObservableField<>();
    public final ObservableField<String> starNumber = new ObservableField<>();
    public final ObservableField<String> forks = new ObservableField<>();

    private Repository repository;
    private OnRepositoryItemListener listener;

    public RepositoryViewModel(Repository repository, OnRepositoryItemListener listener) {
        this.repository = repository;
        this.listener = listener;
        init();
    }

    private void init() {
        repositoryName.set(repository.getFullName());
        description.set(repository.getDescription());
        ownerName.set(repository.getOwner().getLogin());
        ownerAvatar.set(repository.getOwner().getAvatar());
        starNumber.set(String.valueOf(repository.getStars()));
        forks.set(String.valueOf(repository.getForks()));
    }

    public void onItemClicked(){
        listener.onRepositoryClicked(repository);
    }
}

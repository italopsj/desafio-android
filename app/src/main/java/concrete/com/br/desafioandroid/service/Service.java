package concrete.com.br.desafioandroid.service;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import concrete.com.br.desafioandroid.BuildConfig;
import concrete.com.br.desafioandroid.service.interceptor.OfflineResponseCacheInterceptor;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by italopaolo on 15/01/2018.
 */

public class Service {

    private static final int TIME_OUT = 200 * 1000;
    private static final int CONNECTION_TIME_OUT = 200 * 1000;

    private static IService instance;

    public static IService getInstance() {
        if (instance == null) createInstance();

        return instance;
    }

    private static void createInstance() {
        final HashMap<String, String> authHeader = getHeaders();

        /**
         * Log interceptor (just for debug)
         */
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
                .addInterceptor(interceptor)
                .addInterceptor(new OfflineResponseCacheInterceptor())
                .addInterceptor(chain -> {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder();

                    if (!original.url().pathSegments().contains("registrations")) {
                        for (HashMap.Entry<String, String> header : authHeader.entrySet()) {
                            if (header.getValue() != null) {
                                requestBuilder.addHeader(header.getKey(), header.getValue());
                            }
                        }
                    }

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                });


        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        instance = retrofit.create(IService.class);
    }

    private static HashMap<String, String> getHeaders() {
        HashMap<String, String> header = new HashMap<>();

        header.put("Content-Type", "application/json");

        return header;
    }
}

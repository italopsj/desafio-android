package concrete.com.br.desafioandroid.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import concrete.com.br.desafioandroid.App;

/**
 * Created by italopaolo on 17/01/2018.
 */

public class AndroidUtils {

    private static int FADE_DURATION = 120;

    public static final SimpleDateFormat dateFormat =
            new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());

    public static boolean isNetworkAvailable() {
        Context context = App.get();
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService
                (Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getDateHourToShow(String strDate) {
        DateFormat toFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm", Locale.getDefault());
        String formattedDate;
        try {
            Date date = dateFormat.parse(strDate);
            formattedDate = toFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            formattedDate = strDate;
        }
        return formattedDate;
    }

    public static void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(
                0.0f,
                1.0f,
                0.0f,
                1.0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }
}

package concrete.com.br.desafioandroid.viewModel;

import android.databinding.ObservableBoolean;

import com.genius.groupie.GroupAdapter;

import concrete.com.br.desafioandroid.base.BaseViewModel;
import concrete.com.br.desafioandroid.item.ItemPullRequest;
import concrete.com.br.desafioandroid.model.PullRequest;
import concrete.com.br.desafioandroid.model.Repository;
import concrete.com.br.desafioandroid.service.Service;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by italopaolo on 17/01/2018.
 */

public class RepositoryDetailViewModel extends BaseViewModel {

    public final PublishSubject<String> openPullRequestSubject = PublishSubject.create();
    public final ObservableBoolean isLoading = new ObservableBoolean();

    private final GroupAdapter pullRequestAdapter = new GroupAdapter();
    private Repository repository;

    public RepositoryDetailViewModel(Repository repository) {
        this.repository = repository;
        callApi();
    }

    public GroupAdapter getPullRequestAdapter() {
        return pullRequestAdapter;
    }

    private void callApi() {

        Service.getInstance()
                .getPulls(repository.getOwner().getLogin(), repository.getName())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> isLoading.set(true))
                .doOnCompleted(() -> isLoading.set(false))
                .subscribe(response -> {
                    pullRequestAdapter.clear();
                    for (PullRequest pullRequest : response) {
                        pullRequestAdapter.add(new ItemPullRequest(pullRequest, openPullRequestSubject::onNext));
                    }
                }, error -> {
                    error.printStackTrace();
                    isLoading.set(false);
                    errorSubject.onNext(error);
                });
    }

    public interface OnPullRequestListener {
        void onItemClicked(String url);
    }

}

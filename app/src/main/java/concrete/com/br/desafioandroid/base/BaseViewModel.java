package concrete.com.br.desafioandroid.base;

import rx.subjects.PublishSubject;

/**
 * Created by italopaolo on 15/01/2018.
 */

public class BaseViewModel {

    protected PublishSubject<Throwable> errorSubject = PublishSubject.create();

}

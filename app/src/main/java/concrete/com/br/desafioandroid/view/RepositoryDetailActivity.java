package concrete.com.br.desafioandroid.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;

import concrete.com.br.desafioandroid.R;
import concrete.com.br.desafioandroid.base.BaseViewModelActivity;
import concrete.com.br.desafioandroid.databinding.ActivityRepositoryDetailBinding;
import concrete.com.br.desafioandroid.model.Repository;
import concrete.com.br.desafioandroid.viewModel.RepositoryDetailViewModel;
import rx.subscriptions.CompositeSubscription;

public class RepositoryDetailActivity extends
        BaseViewModelActivity<ActivityRepositoryDetailBinding, RepositoryDetailViewModel> {

    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_repository_detail);

        if (viewModel == null) {
            viewModel = new RepositoryDetailViewModel(
                    (Repository) getIntent().getSerializableExtra(getString(R.string.repository_tag)));
        }

        binding.setViewModel(viewModel);

        setUpToolbar(getString(R.string.repository_toolbar_title), true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!subscriptions.hasSubscriptions()) {
            subscriptions.add(viewModel
                    .openPullRequestSubject
                    .subscribe(url -> {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    }));
        }

    }
}

package concrete.com.br.desafioandroid.base;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import concrete.com.br.desafioandroid.R;
import concrete.com.br.desafioandroid.utils.AndroidUtils;
import concrete.com.br.desafioandroid.utils.DialogUtils;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by italopaolo on 15/01/2018.
 */

public class BaseViewModelActivity
        <T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity {

    public Toolbar toolbar;
    protected T binding;
    protected V viewModel;
    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!subscriptions.hasSubscriptions()) {
            subscriptions.add(viewModel
                    .errorSubject
                    .subscribe(this::showErrors));
        }
    }

    public void setUpToolbar(String title, boolean showBackButton) {
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            TextView textView = toolbar.findViewById(R.id.txt_title);
            textView.setText(title);
        }
        if (showBackButton) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AndroidUtils.hideKeyboard(getApplicationContext(), view);
                    finish();
                }
            });
        }
    }

    public void showErrors(Throwable e) {
        e.printStackTrace();
        DialogUtils.alertDialog(this, e.getMessage());
    }

}

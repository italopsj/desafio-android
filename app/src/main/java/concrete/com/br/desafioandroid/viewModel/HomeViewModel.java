package concrete.com.br.desafioandroid.viewModel;

import android.databinding.ObservableBoolean;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.genius.groupie.GroupAdapter;

import concrete.com.br.desafioandroid.base.BaseViewModel;
import concrete.com.br.desafioandroid.model.Repository;
import concrete.com.br.desafioandroid.service.Service;
import concrete.com.br.desafioandroid.item.ItemRepository;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by italopaolo on 15/01/2018.
 */

public class HomeViewModel extends BaseViewModel {

    public final PublishSubject<Repository> repositorySubject = PublishSubject.create();
    public final ObservableBoolean isLoading = new ObservableBoolean();
    public final RecyclerView.OnScrollListener onScrollListener;
    private final GroupAdapter repositoryAdapter = new GroupAdapter();

    private int page = 1;
    private int pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean morePages = true;

    public HomeViewModel() {

        callApi(page);

        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    visibleItemCount = manager.getChildCount();
                    totalItemCount = manager.getItemCount();
                    pastVisibleItems = manager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        if (morePages && !isLoading.get()) {
                            page++;
                            callApi(page);
                        }
                    }
                }
            }
        };
    }

    public GroupAdapter getRepositoryAdapter() {
        return repositoryAdapter;
    }

    private void callApi(int page) {

        Service.getInstance()
                .searchRepositories("language:Java", "stars", page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> isLoading.set(true))
                .doOnCompleted(() -> isLoading.set(false))
                .subscribe(response -> {
                    if (response.getRepositories().size() == 0)
                        morePages = false;
                    if (page == 1)
                        repositoryAdapter.clear();
                    for (Repository repository : response.getRepositories()) {
                        repositoryAdapter.add(
                                new ItemRepository(repository, repositorySubject::onNext));
                    }
                }, error -> {
                    error.printStackTrace();
                    isLoading.set(false);
                    errorSubject.onNext(error);
                });
    }

    public interface OnRepositoryItemListener {
        void onRepositoryClicked(Repository repository);
    }

}

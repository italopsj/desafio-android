package concrete.com.br.desafioandroid.viewModel;

import concrete.com.br.desafioandroid.base.BaseViewModel;
import concrete.com.br.desafioandroid.model.PullRequest;
import concrete.com.br.desafioandroid.utils.AndroidUtils;

import static concrete.com.br.desafioandroid.viewModel.RepositoryDetailViewModel.OnPullRequestListener;

/**
 * Created by italopaolo on 17/01/2018.
 */

public class PullRequestViewModel extends BaseViewModel {

    private final PullRequest pullRequest;
    private final OnPullRequestListener listener;

    public PullRequestViewModel(PullRequest pullRequest, OnPullRequestListener listener) {
        this.pullRequest = pullRequest;
        this.listener = listener;
    }

    public String getTitle(){
        return pullRequest.getTitle();
    }

    public String getCreatedAt(){
        return AndroidUtils.getDateHourToShow(pullRequest.getCreatedAt());
    }

    public String getBody(){
        return pullRequest.getBody();
    }

    public String getOwnerName(){
        return pullRequest.getOwner().getLogin();
    }

    public String getOwnerAvatar(){
        return pullRequest.getOwner().getAvatar();
    }

    public void openPullRequestPage(){
        listener.onItemClicked(pullRequest.getPullUrl());
    }

}

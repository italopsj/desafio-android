package concrete.com.br.desafioandroid.item;

import com.genius.groupie.Item;

import concrete.com.br.desafioandroid.R;
import concrete.com.br.desafioandroid.databinding.ItemPullRequestBinding;
import concrete.com.br.desafioandroid.databinding.ItemRepositoryBinding;
import concrete.com.br.desafioandroid.model.PullRequest;
import concrete.com.br.desafioandroid.model.Repository;
import concrete.com.br.desafioandroid.utils.AndroidUtils;
import concrete.com.br.desafioandroid.viewModel.PullRequestViewModel;
import concrete.com.br.desafioandroid.viewModel.RepositoryDetailViewModel;
import concrete.com.br.desafioandroid.viewModel.RepositoryDetailViewModel.OnPullRequestListener;
import concrete.com.br.desafioandroid.viewModel.RepositoryViewModel;

/**
 * Created by italopaolo on 16/01/2018.
 */

public class ItemPullRequest extends Item<ItemPullRequestBinding> {

    private PullRequestViewModel viewModel;

    public ItemPullRequest(PullRequest pullRequest, OnPullRequestListener listener) {
        viewModel = new PullRequestViewModel(pullRequest, listener);
    }

    @Override
    public int getLayout() {
        return R.layout.item_pull_request;
    }

    @Override
    public void bind(ItemPullRequestBinding viewBinding, int position) {
        AndroidUtils.setScaleAnimation(viewBinding.getRoot());
        viewBinding.setViewModel(viewModel);
    }
}
